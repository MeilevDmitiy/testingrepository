﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Core;
using TestApplication.DataStructures;
using TestApplication.Utils;

namespace TestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var calc = new Calculator();

            double a = 2, b = 3.5;

            Console.WriteLine($"{a} {Constants.AddCommand} {b} = {calc.Commands[Constants.AddCommand].Execute(a, b)}");
            Console.WriteLine($"{a} {Constants.SubtractCommand} {b} = {calc.Commands[Constants.SubtractCommand].Execute(a, b)}");
            Console.WriteLine($"{a} {Constants.MultiplyCommand} {b} = {calc.Commands[Constants.MultiplyCommand].Execute(a, b)}");
            Console.WriteLine($"{a} {Constants.DivideCommand} {b} = {calc.Commands[Constants.DivideCommand].Execute(a, b)}");
        }
    }
}

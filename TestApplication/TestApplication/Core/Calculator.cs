﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Commands;
using TestApplication.DataStructures;
using TestApplication.Utils;

namespace TestApplication.Core
{
    /// <summary>
    /// Provides basic arithmetics commands.
    /// </summary>
    public class Calculator
    {
        #region Properties

        /// <summary>
        /// Contains available arithmetic commands.
        /// </summary>
        public IDictionary<string, ICommand> Commands { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes calculator with commands that defined by AssemblyCommandAttribute
        /// </summary>
        public Calculator()
        {
            var attributes = Assembly.GetExecutingAssembly().GetCustomAttributes<AssemblyCommandAttribute>();
            if (attributes == null)
            {
                throw new NullReferenceException("You must register commands before creating calculator.");
            }

            this.Commands = new CustomDictionary<string, ICommand>(attributes.Count());

            foreach(var a in attributes)
            {
                this.Commands.Add(a.CommandName, (ICommand)Activator.CreateInstance(a.CommandType));
            }
        }

        #endregion
    }
}

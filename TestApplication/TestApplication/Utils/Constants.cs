﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication.Utils
{
    /// <summary>
    /// Set of constants.
    /// </summary>
    public static class Constants
    {
        #region Fields

        /// <summary>
        /// String represents addition.
        /// </summary>
        public const string AddCommand = "+";

        /// <summary>
        /// String represents subtraction.
        /// </summary>
        public const string SubtractCommand = "-";

        /// <summary>
        /// String represents multiplication.
        /// </summary>
        public const string MultiplyCommand = "*";

        /// <summary>
        /// String represents division.
        /// </summary>
        public const string DivideCommand = "/";

        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication.DataStructures
{
    /// <summary>
    /// Represents a generic collection of key/value pairs.
    /// </summary>
    /// <typeparam name="TKey">The type of keys in the dictionary.</typeparam>
    /// <typeparam name="TValue">The type of values in the dictionary.</typeparam>
    public class CustomDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        #region Fields

        private int _count;
        private int _capacity;
        private Entry[] _entries;
        private IEqualityComparer<TKey> _keyComparer;
        private IEqualityComparer<TValue> _valueComparer;
        private const int MaxAverageListLength = 10;
        private const int DefaultCapacity = 1;
        private const int ScaleFactor = 2;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the number of elements contained in the CustomDictionary<TKey, TValue>.
        /// </summary>
        public int Count
        {
            get
            {
                return this._count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the CustomDictionary<TKey, TValue> is read-only.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a collection containing the keys of the CustomDictionary<TKey,TValue>.
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                return this.IterateThroughEntries().Select(e => e.Key).ToList();
            }
        }

        /// <summary>
        /// Gets a collection containing the values in the CustomDictionary<TKey,TValue>.
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                return this.IterateThroughEntries().Select(e => e.Value).ToList();
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the CustomDictionary<TKey,TValue> class that is empty, has the default initial capacity,
        /// and uses the default equality comparer for the key and value types.
        /// </summary>
        public CustomDictionary() : this(DefaultCapacity, null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the CustomDictionary<TKey, TValue> class that is empty, has the specified initial capacity,
        /// and uses the default equality comparer for the key and value type.
        /// </summary>
        /// <param name="capacity">The initial number of elements that the CustomDictionary<TKey,TValue> can contain.</param>
        public CustomDictionary(int capacity) : this(capacity, null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the CustomDictionary<TKey,TValue> class that is empty, has the specified initial capacity, and uses the specified IEqualityComparer<TKey>.
        /// </summary>
        /// <param name="capacity">The initial number of elements that the CustomDictionary<TKey,TValue> can contain.</param>
        /// <param name="keyComparer">The IEqualityComparer<TKey> implementation to use when comparing keys, or null to use the default EqualityComparer<T> for the type of the key.</param>
        public CustomDictionary(int capacity, IEqualityComparer<TKey> keyComparer) : this(capacity, keyComparer, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the CustomDictionary<TKey,TValue> class that is empty, has the specified initial capacity,
        /// and uses the specified IEqualityComparer<TKey> and IEqualityComparer<TValue>.
        /// </summary>
        /// <param name="capacity">The initial number of elements that the CustomDictionary<TKey,TValue> can contain.</param>
        /// <param name="keyComparer">The IEqualityComparer<TKey> implementation to use when comparing keys, or null to use the default EqualityComparer<TKey> for the type of the key.</param>
        /// <param name="valueComparer">The IEqualityComparer<TValue> implementation to use when comparing values, or null to use the default EqualityComparer<TValue> for the type of the value.</param>
        public CustomDictionary(int capacity, IEqualityComparer<TKey> keyComparer, IEqualityComparer<TValue> valueComparer)
        {
            if (capacity <= 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            else
            {
                this.Initialize(capacity);
            }
            this._keyComparer = keyComparer;
            if (this._keyComparer == null)
            {
                this._keyComparer = EqualityComparer<TKey>.Default;
            }
            this._valueComparer = valueComparer;
            if (this._valueComparer == null)
            {
                this._valueComparer = EqualityComparer<TValue>.Default;
            }
        }

        #endregion

        #region Indexers

        /// <summary>
        /// Gets or sets the element with the specified key.
        /// </summary>
        /// <param name="key">The key of the element to get or set.</param>
        /// <returns>The element with the specified key.</returns>
        public TValue this[TKey key]
        {
            get
            {
                TValue value;
                var isValueFound = this.TryGetValue(key, out value);

                if (isValueFound)
                {
                    return value;
                }
                else
                {
                    throw new KeyNotFoundException();
                }
            }

            set
            {
                this.Add(key, value);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Iterating through dictionary entries and returns them as IEnumerable<Entry>.
        /// </summary>
        /// <returns>Collection of entries.</returns>
        private IEnumerable<Entry> IterateThroughEntries()
        {
            foreach (var entry in this._entries)
            {
                for (var collisionEntry = entry; collisionEntry != null; collisionEntry = collisionEntry.Next)
                {
                    yield return collisionEntry;
                }
            }
        }

        /// <summary>
        /// Iterating through dictionary entries and executing specified action on each entry.
        /// </summary>
        /// <param name="processEntry">Action to execute on entry.</param>
        private void IterateThroughEntries(Action<Entry> processEntry)
        {
            if (processEntry == null)
            {
                throw new ArgumentNullException();
            }

            foreach (var entry in this._entries)
            {
                for (var collisionEntry = entry; collisionEntry != null; collisionEntry = collisionEntry.Next)
                {
                    processEntry(collisionEntry);
                }
            }
        }

        /// <summary>
        /// Initializes dictionary with capacity.
        /// </summary>
        /// <param name="capacity">Size of the dictionary.</param>
        private void Initialize(int capacity)
        {
            this._capacity = capacity;
            this._count = 0;
            this._entries = new Entry[capacity];
        }

        /// <summary>
        /// Resizes dictionary to new size.
        /// </summary>
        /// <param name="newSize">New size.</param>
        private void Resize(int newSize)
        {
            CustomDictionary<TKey, TValue> temp = new CustomDictionary<TKey, TValue>(newSize);

            this.IterateThroughEntries(e => temp.Add(e.Key, e.Value));

            this._capacity = temp._capacity;
            this._count = temp._count;
            this._entries = temp._entries;
        }

        /// <summary>
        /// Adds an element with the provided key/value pair to the CustomDictionary<TKey, TValue>.
        /// </summary>
        /// <param name="item">The element that will be added.</param>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            Add(item.Key, item.Value);
        }

        /// <summary>
        /// Adds an element with the provided key and value to the CustomDictionary<TKey, TValue>.
        /// </summary>
        /// <param name="key">The object to use as the key of the element to add.</param>
        /// <param name="value">The object to use as the value of the element to add.</param>
        public void Add(TKey key, TValue value)
        {
            if (key == null)
            {
                throw new ArgumentNullException();
            }
            
            if (this._count >= MaxAverageListLength * this._capacity)
            {
                this.Resize(ScaleFactor * this._capacity);
            }

            int hash = this.GetKeyHash(key);

            Entry prevEntry = null;
            for (var entry = this._entries[hash]; entry != null; entry = entry.Next)
            {
                if (entry != null)
                {
                    prevEntry = entry;
                }
                if (this._keyComparer.Equals(key, entry.Key))
                {
                    entry.Value = value;

                    return;
                }
            }
            if (prevEntry != null)
            {
                prevEntry.Next = new Entry(key, value);
            }
            else
            {
                this._entries[hash] = new Entry(key, value, this._entries[hash]);
            }
            this._count++;
        }

        /// <summary>
        /// Removes all items from the CustomDictionary<TKey, TValue>.
        /// </summary>
        public void Clear()
        {
            this.Initialize(DefaultCapacity);
        }

        /// <summary>
        /// Determines whether the CustomDictionary<TKey, TValue> contains a specific value.
        /// </summary>
        /// <param name="item">The object to locate in the CustomDictionary<TKey, TValue>.</param>
        /// <returns>True if item is found in the CustomDictionary<TKey, TValue>; otherwise, false.</returns>
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            TValue value;
            
            if (this.TryGetValue(item.Key, out value))
            {
                return this._valueComparer.Equals(item.Value, value);
            }

            return false;
        }

        /// <summary>
        /// Determines whether the CustomDictionary<TKey,TValue> contains an element with the specified key.
        /// </summary>
        /// <param name="key">The key to locate in the CustomDictionary<TKey,TValue>.</param>
        /// <returns>True if item is found in the CustomDictionary<TKey, TValue>; otherwise, false.</returns>
        public bool ContainsKey(TKey key)
        {
            TValue value;

            return this.TryGetValue(key, out value);
        }

        /// <summary>
        /// Copies the elements of the CustomDictionary<TKey, TValue> to an Array, starting at a particular Array index.
        /// </summary>
        /// <param name="array">The one-dimensional Array that is the destination of the elements copied from CustomDictionary<TKey, TValue>. The Array must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in array at which copying begins.</param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int index)
        {
            if (array == null)
            {
                throw new ArgumentNullException();
            }

            if (index < 0 || index > array.Length)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (array.Length - index < this._count)
            {
                throw new ArgumentException("Array is too small.");
            }

            this.IterateThroughEntries(e => array[index++] = new KeyValuePair<TKey, TValue>(e.Key, e.Value));
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An IEnumerator object that can be used to iterate through the collection.</returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Removes the element with the specified key/value pair from the CustomDictionary<TKey,TValue>.
        /// </summary>
        /// <param name="item">The key/value pair to remove.</param>
        /// <returns>True if the element is successfully removed; otherwise, false. This method also returns false if key was not found in the original CustomDictionary<TKey,TValue>.</returns>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return Remove(item.Key);
        }

        /// <summary>
        /// Removes the element with the specified key from the CustomDictionary<TKey,TValue>.
        /// </summary>
        /// <param name="key">The key of the element to remove.</param>
        /// <returns>True if the element is successfully removed; otherwise, false. This method also returns false if key was not found in the original CustomDictionary<TKey,TValue>.</returns>
        public bool Remove(TKey key)
        {
            if (key == null)
            {
                throw new ArgumentNullException();
            }

            int hash = this.GetKeyHash(key);


            Entry prevEntry = this._entries[hash];
            if (prevEntry != null)
            {
                if (this._keyComparer.Equals(key, prevEntry.Key))
                {
                    this._entries[hash] = null;
                    this._count--;
                    if (this._capacity > DefaultCapacity && this._count <= ScaleFactor * this._capacity)
                    {
                        this.Resize(this._capacity / ScaleFactor);
                    }

                    return true;
                }
                for (var entry = prevEntry.Next; entry != null; entry = entry.Next)
                {
                    if (this._keyComparer.Equals(key, entry.Key))
                    {
                        prevEntry.Next = null;
                        this._count--;
                        if (this._capacity > DefaultCapacity && this._count <= ScaleFactor * this._capacity)
                        {
                            this.Resize(this._capacity / ScaleFactor);
                        }

                        return true;
                    }
                    else
                    {
                        prevEntry = entry;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key whose value to get.</param>
        /// <param name="value">
        /// When this method returns, contains the value associated with the specified key, if the key is found; otherwise,
        /// the default value for the type of the value parameter. This parameter is passed uninitialized.
        /// </param>
        /// <returns>True if the object that implements CustomDictionary<TKey,TValue> contains an element with the specified key; otherwise, false.</returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            if (key == null)
            {
                throw new ArgumentNullException();
            }

            int hash = this.GetKeyHash(key);

            for (var entry = this._entries[hash]; entry != null; entry = entry.Next)
            {
                if (this._keyComparer.Equals(key, entry.Key))
                {
                    value = entry.Value;

                    return true;
                }
            }
            value = default(TValue);

            return false;
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An IEnumerator object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Get hash code of the specified key.
        /// </summary>
        /// <param name="key">The object for which a hash code is to be returned.</param>
        /// <returns>A hash code for the specified object.</returns>
        private int GetKeyHash(TKey key)
        {
            return (this._keyComparer.GetHashCode(key) & int.MaxValue) % this._capacity;
        }

        #endregion

        #region Nested types

        /// <summary>
        /// Represents stored data.
        /// </summary>
        private class Entry
        {
            #region Properties

            public TKey Key { get; set; }
            public TValue Value { get; set; }

            /// <summary>
            /// Reference to next entry in the linked list.
            /// </summary>
            public Entry Next { get; set; }

            #endregion

            #region Constructors

            /// <summary>
            /// Initializes CustomDictionary<TKey, TValue> entry which stores key/value pair and reference to next entry if collision appears.
            /// </summary>
            /// <param name="key">The object to use as the key of the element to store.</param>
            /// <param name="value">The object to use as the value of the element to store.</param>
            /// <param name="next">Reference to next entry in the linked list.</param>
            public Entry(TKey key, TValue value, Entry next = null)
            {
                this.Key = key;
                this.Value = value;
                this.Next = next;
            }

            #endregion
        }

        /// <summary>
        /// CustomDictionary enumerator used for iterating through key/value entries.
        /// </summary>
        private struct Enumerator : IEnumerator<KeyValuePair<TKey, TValue>>
        {
            #region Fields

            private CustomDictionary<TKey, TValue> _dictionary;
            private Entry _currentEntry;
            private int _index;

            #endregion

            #region Constructors

            /// <summary>
            /// Initializes Enumerator with related CustomDictionary<TKey, TValue>.
            /// </summary>
            /// <param name="dictionary">CustomDictionary<TKey, TValue> to be enumerated.</param>
            public Enumerator(CustomDictionary<TKey, TValue> dictionary)
            {
                this._dictionary = dictionary;
                this._index = 0;
                this._currentEntry = null;
            }

            #endregion

            #region Properties

            /// <summary>
            /// Gets the element in the collection at the current position of the enumerator.
            /// </summary>
            public KeyValuePair<TKey, TValue> Current
            {
                get
                {
                    if (this._currentEntry != null)
                    {
                        return new KeyValuePair<TKey, TValue>(this._currentEntry.Key, this._currentEntry.Value);
                    }
                    else
                    {
                        return new KeyValuePair<TKey, TValue>();
                    }
                }
            }

            /// <summary>
            /// Gets the element in the collection at the current position of the enumerator.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    return Current;
                }
            }

            #endregion

            #region Methods

            /// <summary>
            /// Part of IEnumerator<KeyValuePair<TKey, TValue>> interface, not needed.
            /// </summary>
            public void Dispose()
            {
            }

            /// <summary>
            /// Advances the enumerator to the next element of the collection.
            /// </summary>
            /// <returns>True if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.</returns>
            public bool MoveNext()
            {
                while (this._index < this._dictionary._entries.Length)
                {
                    if (this._dictionary._entries[this._index] != null)
                    {
                        if (this._currentEntry == null)
                        {
                            this._currentEntry = this._dictionary._entries[this._index];

                            return true;
                        }
                        else
                        {
                            this._currentEntry = this._currentEntry.Next;

                            if (this._currentEntry == null)
                            {
                                this._index++;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        this._index++;
                    }
                }

                this._index = this._dictionary._entries.Length + 1;
                this._currentEntry = null;

                return false;
            }

            /// <summary> 	
            /// Sets the enumerator to its initial position, which is before the first element in the collection.
            /// </summary>
            public void Reset()
            {
                this._currentEntry = null;
                this._index = 0;
            }

            #endregion
        }

        #endregion
    }
}

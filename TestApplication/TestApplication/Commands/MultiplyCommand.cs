﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Commands;
using TestApplication.Utils;

// Register multiplication command defined by the following class.
[assembly: AssemblyCommand(Constants.MultiplyCommand, typeof(MultiplyCommand))]
namespace TestApplication.Commands
{
    /// <summary>
    /// Implements multiplication command.
    /// </summary>
    public class MultiplyCommand : ICommand
    {
        #region Methods

        /// <summary>
        /// Execute multiplication command.
        /// </summary>
        /// <param name="a">Left operand.</param>
        /// <param name="b">Right operand.</param>
        /// <returns>Result of multiplication.</returns>
        public double Execute(double a, double b)
        {
            return a * b;
        }

        #endregion
    }
}

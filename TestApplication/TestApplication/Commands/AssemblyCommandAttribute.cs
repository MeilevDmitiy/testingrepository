﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication.Commands
{
    /// <summary>
    /// Assembly attribute for registering arithmetic commands by specified name and type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
    public class AssemblyCommandAttribute : Attribute
    {
        #region Properties

        public string CommandName { get; }
        public Type CommandType { get; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the AssemblyCommandAttribute class with a arithemtic command name and type.
        /// </summary>
        /// <param name="commandName">Name of the command.</param>
        /// <param name="commandType">Type of the command.</param>
        public AssemblyCommandAttribute(string commandName, Type commandType)
        {
            if (commandName == null || commandType == null)
            {
                throw new ArgumentNullException();
            }

            this.CommandName = commandName;
            this.CommandType = commandType;
        }

        #endregion
    }
}

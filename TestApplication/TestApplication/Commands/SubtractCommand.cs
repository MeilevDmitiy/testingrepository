﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Commands;
using TestApplication.Utils;

// Register subtraction command defined by the following class.
[assembly: AssemblyCommand(Constants.SubtractCommand, typeof(SubtractCommand))]
namespace TestApplication.Commands
{
    /// <summary>
    /// Implements subtraction command.
    /// </summary>
    public class SubtractCommand : ICommand
    {
        #region Methods

        /// <summary>
        /// Execute subtraction command.
        /// </summary>
        /// <param name="a">Left operand.</param>
        /// <param name="b">Right operand.</param>
        /// <returns>Result of subtraction.</returns>
        public double Execute(double a, double b)
        {
            return a - b;
        }

        #endregion
    }
}

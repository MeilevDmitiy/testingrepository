﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Commands;
using TestApplication.Utils;

// Register addition command defined by the following class.
[assembly: AssemblyCommand(Constants.AddCommand, typeof(AddCommand))]
namespace TestApplication.Commands
{
    /// <summary>
    /// Implements addition command.
    /// </summary>
    public class AddCommand : ICommand
    {
        #region Methods

        /// <summary>
        /// Execute addition command.
        /// </summary>
        /// <param name="a">Left operand.</param>
        /// <param name="b">Right operand.</param>
        /// <returns>Result of addition.</returns>
        public double Execute(double a, double b)
        {
            return a + b;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Commands;
using TestApplication.Utils;

// Register division command defined by the following class.
[assembly: AssemblyCommand(Constants.DivideCommand, typeof(DivideCommand))]
namespace TestApplication.Commands
{
    /// <summary>
    /// Implements division command.
    /// </summary>
    public class DivideCommand : ICommand
    {
        #region Methods

        /// <summary>
        /// Execute division command.
        /// </summary>
        /// <param name="a">Left operand.</param>
        /// <param name="b">Right operand.</param>
        /// <returns>Result of division.</returns>
        public double Execute(double a, double b)
        {
            return a / b;
        }

        #endregion
    }
}

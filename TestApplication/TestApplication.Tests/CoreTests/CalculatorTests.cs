﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestApplication.Core;
using TestApplication.Utils;
using TestApplication.Tests.CoreTests;
using TestApplication.Commands;

namespace TestApplication.Tests
{
    [TestClass]
    public class CalculatorTests
    {
        public const string MagicString = "Test";
        public const double MagicNumber = 1337;

        private static Calculator _calculator;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _calculator = new Calculator();
        }

        [TestMethod]
        public void Add_2_And_3_Returns_5()
        {
            // Arrange
            const double Expected = 5;
            double a = 2, b = 3;

            // Act
            var actual = _calculator.Commands[Constants.AddCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Add_Minus5_And_3_Returns_Minus2()
        {
            // Arrange
            const double Expected = -2;
            double a = -5, b = 3;

            // Act
            var actual = _calculator.Commands[Constants.AddCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Add_Minus5_And_Minus3_Returns_Minus8()
        {
            // Arrange
            const double Expected = -8;
            double a = -5, b = -3;

            // Act
            var actual = _calculator.Commands[Constants.AddCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Subtract_2_And_3_Returns_Minus1()
        {
            // Arrange
            const double Expected = -1;
            double a = 2, b = 3;

            // Act
            var actual = _calculator.Commands[Constants.SubtractCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Subtract_Minus2_And_3_Returns_Minus5()
        {
            // Arrange
            const double Expected = -5;
            double a = -2, b = 3;

            // Act
            var actual = _calculator.Commands[Constants.SubtractCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Subtract_Minus2_And_Minus3_Returns_1()
        {
            // Arrange
            const double Expected = 1;
            double a = -2, b = -3;

            // Act
            var actual = _calculator.Commands[Constants.SubtractCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Multiply_2_And_3_Returns_6()
        {
            // Arrange
            const double Expected = 6;
            double a = 2, b = 3;

            // Act
            var actual = _calculator.Commands[Constants.MultiplyCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Multiply_Minus2_And_3_Returns_Minus6()
        {
            // Arrange
            const double Expected = -6;
            double a = -2, b = 3;

            // Act
            var actual = _calculator.Commands[Constants.MultiplyCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Multiply_Minus2_And_Minus3_Returns_6()
        {
            // Arrange
            const double Expected = 6;
            double a = -2, b = -3;

            // Act
            var actual = _calculator.Commands[Constants.MultiplyCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Divide_6_And_3_Returns_2()
        {
            // Arrange
            const double Expected = 2;
            double a = 6, b = 3;

            // Act
            var actual = _calculator.Commands[Constants.DivideCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Divide_Minus6_And_3_Returns_Minus2()
        {
            // Arrange
            const double Expected = -2;
            double a = -6, b = 3;

            // Act
            var actual = _calculator.Commands[Constants.DivideCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Divide_Minus6_And_Minus3_Returns_2()
        {
            // Arrange
            const double Expected = 2;
            double a = -6, b = -3;

            // Act
            var actual = _calculator.Commands[Constants.DivideCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Divide_6_And_0_Returns_PositiveInfinity()
        {
            // Arrange
            const double Expected = double.PositiveInfinity;
            double a = 6, b = 0;

            // Act
            var actual = _calculator.Commands[Constants.DivideCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        public void Divide_Minus6_And_0_Returns_NegativeInfinity()
        {
            // Arrange
            const double Expected = double.NegativeInfinity;
            double a = -6, b = 0;

            // Act
            var actual = _calculator.Commands[Constants.DivideCommand].Execute(a, b);

            // Assert
            Assert.AreEqual(Expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Collections.Generic.KeyNotFoundException))]
        public void Execute_NonExistentCommand_Throws_KeyNotFoundException()
        {
            // Arrange
            double a = -6, b = 0;

            // Act
            var actual = _calculator.Commands["SUPER COOL ARITHMETIC COMMAND"].Execute(a, b);

            // Assert - Expects exception
        }

        [TestMethod]
        public void FindAndExecute_TestCommand_Returns_MagicNumber()
        {
            // Arrange - See TestCommand.cs
            double a, b, expected = MagicNumber;
            a = b = 0;

            // Act
            var actual = _calculator.Commands[MagicString].Execute(a, b);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Add_TestCommand_Directly_Via_CommandsDictionary_Execute_Returns_MagicNumber()
        {
            // Arrange
            ICommand testCommand = new TestCommand();
            double a, b, expected = MagicNumber;
            a = b = 0;

            // Act
            _calculator.Commands.Add(MagicString, testCommand);
            var actual = _calculator.Commands[MagicString].Execute(a, b);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.Commands;
using TestApplication.Tests;
using TestApplication.Tests.CoreTests;

[assembly: AssemblyCommand(CalculatorTests.MagicString, typeof(TestCommand))]
namespace TestApplication.Tests.CoreTests
{
    public class TestCommand : ICommand
    {
        public double Execute(double a, double b)
        {
            return CalculatorTests.MagicNumber;
        }
    }
}

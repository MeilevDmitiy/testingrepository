﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication.Tests.DataStructuresTests
{
    public class TestReferenceTypeEqualityComparer : IEqualityComparer<string>
    {
        public bool Equals(string x, string y)
        {
            return x.Length != y.Length - 1;
        }

        public int GetHashCode(string obj)
        {
            return obj.Length;
        }
    }
}

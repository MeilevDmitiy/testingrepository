﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestApplication.DataStructures;
using System.Collections.Generic;
using System.Linq;

namespace TestApplication.Tests.DataStructuresTests
{
    [TestClass]
    public class CustomDictionaryTests
    {
        private CustomDictionary<int, int> _intDict;
        private CustomDictionary<string, string> _stringDict;

        [TestInitialize]
        public void Init()
        {
            this._intDict = new CustomDictionary<int, int>();
            this._stringDict = new CustomDictionary<string, string>();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Initialize_With_Zero_Capacity_Throws_ArgumentOutOfRangeException()
        {
            // Arrange + Act
            this._intDict = new CustomDictionary<int, int>(0);

            // Assert - Expects exception
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Initialize_With_Negative_Capacity_Throws_ArgumentOutOfRangeException()
        {
            // Arrange + Act
            this._intDict = new CustomDictionary<int, int>(-1);

            // Assert - Expects exception
        }

        [TestMethod]
        public void Add_ValueType_Items_Count_Equals_3()
        {
            // Arrange
            int expectedCount = 3;

            // Act
            for (var i = 0; i < expectedCount; ++i)
            {
                this._intDict.Add(i, i);
            }

            // Assert
            Assert.AreEqual(expectedCount, this._intDict.Count);
        }

        [TestMethod]
        public void Add5Entries_Get3_Returns3()
        {
            // Arrange
            int expected = 3;

            // Act
            for (var i = 1; i <= 5; ++i)
            {
                this._intDict.Add(i, i);
            }

            // Assert
            Assert.AreEqual(expected, this._intDict[expected]);
        }

        [TestMethod]
        public void Add5Entries_TryGet3_ReturnsTrueAndActualEquals3()
        {
            // Arrange
            int expected = 3;

            // Act
            for (var i = 1; i <= 5; ++i)
            {
                this._intDict.Add(i, i);
            }

            // Assert
            int actual;
            Assert.IsTrue(this._intDict.TryGetValue(expected, out actual) && actual == expected);
        }

        [TestMethod]
        public void Remove_NonExistentValueTypeKey_ReturnsFalse()
        {
            // Arrange - Init()

            // Act
            var result = this._intDict.Remove(1);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Remove_NonExistentReferenceTypeKey_ReturnsFalse()
        {
            // Arrange - Init()

            // Act
            var result = this._stringDict.Remove("FoundKey");

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void Remove_ExistentValueTypeKey_ReturnsTrue()
        {
            // Arrange
            int key = 1;
            this._intDict.Add(key, 1);

            // Act
            var result = this._intDict.Remove(key);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Remove_ExistentReferenceTypeKey_ReturnsTrue()
        {
            // Arrange
            string key = "1";
            this._stringDict.Add(key, "qwe");

            // Act
            var result = this._stringDict.Remove(key);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Add_DifferentValues_With_SameValueTypeKey_Returns_Last()
        {
            // Arrange
            int key = 1;
            int value1 = 2,
                value2 = 3,
                expectedValue = value2;

            // Act
            this._intDict.Add(key, value1);
            this._intDict.Add(key, value2);

            // Assert
            Assert.AreEqual(expectedValue, this._intDict[key]);
        }

        [TestMethod]
        public void Add_DifferentValues_With_SameReferenceTypeKey_Returns_Last()
        {
            // Arrange
            string key = "1";
            string value1 = "asd",
                   value2 = "dsa",
                   expectedValue = value2;

            // Act
            this._stringDict.Add(key, value1);
            this._stringDict.Add(key, value2);

            // Assert
            Assert.AreEqual(expectedValue, this._stringDict[key]);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Add_NullKey_Throws_ArgumentNullException()
        {
            // Arrange - Init()

            // Act
            this._stringDict.Add(null, null);

            // Assert - Expects exception
        }

        [TestMethod]
        public void Add_NullValue_Count_Equals_1()
        {
            // Arrange
            string key = "1";
            int expectedCount = 1;

            // Act
            this._stringDict.Add(key, null);

            // Assert
            Assert.AreEqual(expectedCount, this._stringDict.Count);
        }

        [TestMethod]
        public void Clear_DictWithEntries_Count_Equals_0()
        {
            // Arrange
            int expectedCount = 0;
            for (var i = 0; i < 10; ++i)
            {
                this._intDict.Add(i, i);
            }

            // Act
            this._intDict.Clear();

            // Assert
            Assert.AreEqual(expectedCount, this._intDict.Count);
        }

        [TestMethod]
        public void Add_DifferentValues_With_SameKey_Contains_Last()
        {
            // Arrange
            int key = 1,
                value1 = 1,
                value2 = 2;
            this._intDict.Add(key, value1);
            this._intDict.Add(key, value2);

            // Act
            var contains = this._intDict.Contains(new KeyValuePair<int, int>(key, value2));

            // Assert
            Assert.IsTrue(contains);
        }

        [TestMethod]
        public void NonExistentKey_ContainsKey_Returns_False()
        {
            // Arrange - Init()

            // Act
            var containsKey = this._stringDict.ContainsKey("This key exists for sure.");

            // Assert
            Assert.IsFalse(containsKey);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void DictionaryWith5Entries_CopyToSmallArray_Throws_ArgumentException()
        {
            // Arrange
            int size = 5;
            for (var i = 0; i < size; ++i)
            {
                this._intDict.Add(i, i);
            }
            KeyValuePair<int, int>[] destination = new KeyValuePair<int, int>[size - 1];

            // Act
            this._intDict.CopyTo(destination, 0);

            // Assert - Expects exception
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void DictionaryWith5Entries_CopyToMidOfArray_Throws_ArgumentException()
        {
            // Arrange
            int size = 5;
            for (var i = 0; i < size; ++i)
            {
                this._intDict.Add(i, i);
            }
            KeyValuePair<int, int>[] destination = new KeyValuePair<int, int>[size];

            // Act
            this._intDict.CopyTo(destination, size / 2);

            // Assert - Expects exception
        }

        [TestMethod]
        public void KeysProperty_ReturnsCorrectCollection()
        {
            // Arrange
            this._stringDict.Add("1", "1");
            this._stringDict.Add("3", "2");
            this._stringDict.Add("2", "3");
            var expected = new[] { "1", "2", "3" };

            // Act
            var actual = this._stringDict.Keys.OrderBy(x => x).ToList();

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ValuesProperty_ReturnsCorrectCollection()
        {
            // Arrange
            this._stringDict.Add("1", "1");
            this._stringDict.Add("3", "2");
            this._stringDict.Add("2", "3");
            var expected = new[] { "1", "2", "3" };

            // Act
            var actual = this._stringDict.Values.OrderBy(x => x).ToList();

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsReadOnlyProperty_ReturnsFalse()
        {
            // Arrange - Init()

            // Act
            var actual = this._intDict.IsReadOnly;

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void AccessTo_NonExistentKey_Throws_KeyNotFoundException()
        {
            // Arrange - Init()

            // Act
            var actual = this._intDict[0];

            // Assert - Expects exception
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AccessTo_NullKey_Throws_ArgumentNullException()
        {
            // Arrange - Init()

            // Act
            var actual = this._stringDict[null];

            // Assert - Expects exception
        }

        [TestMethod]
        public void DictionaryWith5Entries_CopyToArray_ItemsAreEqual()
        {
            // Arrange
            int size = 5;
            for (var i = 0; i < size; ++i)
            {
                this._intDict.Add(i, i);
            }
            var expected = this._intDict.Keys.OrderBy(x => x).Zip(this._intDict.Values.OrderBy(x => x), (a, b) => new KeyValuePair<int, int>(a, b)).ToArray();
            KeyValuePair<int, int>[] actual = new KeyValuePair<int, int>[size];

            // Act
            this._intDict.CopyTo(actual, 0);

            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SuccessfullForeach()
        {
            // Arrange
            int size = 100;
            this._stringDict = new CustomDictionary<string, string>(size);
            var expected = new List<KeyValuePair<string, string>>(size);
            for (var i = 0; i < size; ++i)
            {
                var tmp = i.ToString();
                this._stringDict.Add(tmp, tmp);
                expected.Add(new KeyValuePair<string, string>(tmp, tmp));
            }

            // Act
            var isContains = true;
            foreach(var entry in this._stringDict)
            {
                if (!expected.Contains(entry))
                {
                    isContains = false;

                    break;
                }
            }

            // Assert
            Assert.IsTrue(isContains);
        }

        [TestMethod]
        public void Init_Dictionary_With_TestValueTypeEqualityComparer_AddTwoEntriesWithSameKey_Returns_Last()
        {
            // Arrange
            this._intDict = new CustomDictionary<int, int>(1, new TestValueTypeEqualityComparer());
            var expected = 2;

            // Act
            this._intDict.Add(1, 1);
            this._intDict.Add(-1, 2);
            var actual = this._intDict[-1]; // Key -1 does not exist in dictionary but TestValueTypeEqualityComparer.Equals(int a, int b) returns true if a < b
                                            // so adding entry with key -1 actually updates entry with key 1.

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Init_Dictionary_With_TestReferenceTypeEqualityComparer_AddTwoEntriesWithSameKey_Returns_Last()
        {
            // Arrange
            this._stringDict = new CustomDictionary<string, string>(1, new TestReferenceTypeEqualityComparer(), new TestReferenceTypeEqualityComparer());
            var expected = "2";

            // Act
            this._stringDict.Add("1", "1");
            this._stringDict.Add("2", "2");
            var actual = this._stringDict["2"]; // Key "2" does not exist in dictionary but
                                                // TestReferenceTypeEqualityComparer.Equals(string a, string b) returns true if a.Length != b.Length -1
                                                // so adding entry with key "2" actually updates entry with key "1"

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}

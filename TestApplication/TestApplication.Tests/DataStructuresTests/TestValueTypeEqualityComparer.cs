﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication.Tests.DataStructuresTests
{
    public class TestValueTypeEqualityComparer : IEqualityComparer<int>
    {
        public bool Equals(int x, int y)
        {
            return x < y;
        }

        public int GetHashCode(int obj)
        {
            return obj * 31;
        }
    }
}
